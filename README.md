# Displat - Discord Load Balancer

Displat is a suite of OSS tools to enable scaling Discord bots with ease.

The Displat load balancer is intended as a drop-in replacement for the Discord API to function as a remote cache and a gateway connection independent of your deployments.
