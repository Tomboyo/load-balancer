defmodule LoadBalancer.MixProject do
  use Mix.Project

  def project do
    [
      app: :displat_lb,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {LoadBalancer.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5.0", only: [:dev, :test], runtime: false},
      {:gun, "~> 1.3"},
      {:poison, "~> 4.0"},
      {:junit_formatter, "~> 3.1", only: [:test]},
      {:finch, "~> 0.5"}
    ]
  end
end
