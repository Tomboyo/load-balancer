defmodule LoadBalancer.Discord.Types.Embed.Video do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-video-structure)"

  defstruct [
    :url,
    :height,
    :width
  ]

  @typedoc "Source URL of the video"
  @type url :: String.t() | nil

  @typedoc "Height of the video"
  @type height :: integer | nil

  @typedoc "Width of the video"
  @type width :: integer | nil

  @type t :: %__MODULE__{
          url: url,
          height: height,
          width: width
        }
end
