defmodule LoadBalancer.Discord.Types.Embed.Thumbnail do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-thumbnail-structure)"

  defstruct [
    :url,
    :proxy_url,
    :height,
    :width
  ]

  @typedoc "Source URL of the thumbnail"
  @type url :: String.t() | nil

  @typedoc "URL of thumbnail icon"
  @type proxy_url :: String.t() | nil

  @typedoc "Height of the thumbnail"
  @type height :: integer | nil

  @typedoc "Width of the thumbnail"
  @type width :: integer | nil

  @type t :: %__MODULE__{
          url: url,
          proxy_url: proxy_url,
          height: height,
          width: width
        }
end
