defmodule LoadBalancer.Discord.Types.Embed.Provider do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-provider-structure)"

  defstruct [
    :name,
    :url
  ]

  @typedoc "Name of the provider"
  @type name :: String.t() | nil

  @typedoc "URL of provider"
  @type url :: String.t() | nil

  @type t :: %__MODULE__{
          name: name,
          url: url
        }
end
