defmodule LoadBalancer.Discord.Types.Event.ClientStatus do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#client-status-object)"

  defstruct [
    :desktop,
    :mobile,
    :web
  ]

  @typedoc "the user's status set for an active desktop (Windows, Linux, Mac) application session"
  @type desktop :: String.t() | nil

  @typedoc "the user's status set for an active mobile (iOS, Android) application session"
  @type mobile :: String.t() | nil

  @typedoc "the user's status set for an active web (browser, bot account) application session"
  @type web :: String.t() | nil

  @type t :: %__MODULE__{
          desktop: desktop,
          mobile: mobile,
          web: web
        }
end
