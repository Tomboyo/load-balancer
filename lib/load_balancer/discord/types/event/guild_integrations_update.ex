defmodule LoadBalancer.Discord.Types.Event.GuildIntegrationsUpdate do
  @moduledoc "See [the documentaiton](https://discord.com/developers/docs/topics/gateway#guild-integrations-update-guild-integrations-update-event-fields)"

  alias LoadBalancer.Discord.Types.Guild

  defstruct [:guild_id]

  @typedoc "id of the guild whose integrations were updated"
  @type guild_id :: Guild.id()

  @type t :: %__MODULE__{guild_id: guild_id}
end
