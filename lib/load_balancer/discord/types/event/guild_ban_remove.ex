defmodule LoadBalancer.Discord.Types.Event.GuildBanRemove do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#guild-ban-remove)"

  alias LoadBalancer.Discord.Types.{Guild, User}

  defstruct [
    :guild_id,
    :user
  ]

  @typedoc "id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "the unbanned user"
  @type user :: User.t()

  @type t :: %__MODULE__{
          guild_id: guild_id,
          user: user
        }
end
