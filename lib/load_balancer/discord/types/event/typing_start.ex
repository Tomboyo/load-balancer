defmodule LoadBalancer.Discord.Types.Event.TypingStart do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#typing-start)"

  alias LoadBalancer.Discord.Types.{Channel, Guild, User}
  alias LoadBalancer.Discord.Types.Guild.Member

  defstruct [
    :channel_id,
    :guild_id,
    :user_id,
    :timestamp,
    :member
  ]

  @typedoc "id of the channel"
  @type channel_id :: Channel.id()

  @typedoc "id of the guild"
  @type guild_id :: Guild.id() | nil

  @typedoc "id of the user"
  @type user_id :: User.id()

  @typedoc "unix time (in seconds) of when the user started typing"
  @type timestamp :: integer()

  @typedoc "the member who started typing if this happened in a guild"
  @type member :: Member.t() | nil

  @type t :: %__MODULE__{
          channel_id: channel_id,
          guild_id: guild_id,
          user_id: user_id,
          timestamp: timestamp,
          member: member
        }
end
