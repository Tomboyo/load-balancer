defmodule LoadBalancer.Discord.Types.Event.Ready do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#ready)"

  alias LoadBalancer.Discord.Types.{Guild, User}
  alias LoadBalancer.Discord.Types.Guild.Integration.Application

  defstruct [
    :v,
    :user,
    :private_channels,
    :guilds,
    :session_id,
    :shard,
    :application
  ]

  @typedoc "gateway version"
  @type v :: integer

  @typedoc "information about the user including email"
  @type user :: User.t()

  @typedoc "empty array"
  @type private_channels :: []

  @typedoc "the guilds the user is in"
  @type guilds :: [Guild.t()]

  @typedoc "used for resuming connections"
  @type session_id :: String.t()

  @typedoc "the shard information associated with this session, if sent when identifying. An array of two integers (shard_id, num_shards)"
  @type shard :: [integer] | nil

  @typedoc "Partical application object containing id and flags"
  @type application :: Application.t()

  @type t :: %__MODULE__{
          v: v,
          user: user,
          private_channels: private_channels,
          guilds: guilds,
          session_id: session_id,
          shard: shard,
          application: application
        }
end
