defmodule LoadBalancer.Discord.Types.Event.PresenceUpdate do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#presence)"

  alias LoadBalancer.Discord.Types.{Guild, User}
  alias LoadBalancer.Discord.Types.Event.{Activity, ClientStatus}

  defstruct [
    :user,
    :guild_id,
    :status,
    :activities,
    :client_status
  ]

  @typedoc "the user presence is being updated for"
  @type user :: User.t()

  @typedoc "id of the guild"
  @type guild_id :: Guild.id()

  @typedoc ~S{either "idle", "dnd", "online", or "offline"}
  @type status :: String.t()

  @typedoc "user's current activities"
  @type activities :: [Activity.t()]

  @typedoc "user's platform-dependent status"
  @type client_status :: ClientStatus.t()

  @type t :: %__MODULE__{
          user: user,
          guild_id: guild_id,
          status: status,
          activities: activities,
          client_status: client_status
        }
end
