defmodule LoadBalancer.Discord.Types.Guild.Integration do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#integration-object)"

  alias LoadBalancer.Discord.Types.{Snowflake, User}
  alias LoadBalancer.Discord.Types.Guild.Integration.{Account, Application}
  alias LoadBalancer.Discord.Types.Guild.Role

  defstruct [
    :id,
    :name,
    :type,
    :enabled,
    :syncing,
    :role_id,
    :enable_emoticons,
    :expire_behavior,
    :expire_grace_period,
    :user,
    :account,
    :synced_at,
    :subscriber_count,
    :revoked,
    :application
  ]

  @typedoc "integration id"
  @type id :: Snowflake.t()

  @typedoc "integration name"
  @type name :: String.t()

  @typedoc "integration type (twitch, youtube, or discord)"
  @type type :: String.t()

  @typedoc "is this integration enabled"
  @type enabled :: boolean

  @typedoc "is this integration syncing"
  @type syncing :: boolean

  @typedoc "id that this integration uses for \"subscribers\""
  @type role_id :: Role.id()

  @typedoc "whether emoticons should be synced for this integration (twitch only currently)"
  @type enable_emoticons :: boolean

  @typedoc "the behavior of expiring subscribers"
  @type expire_behavior :: remove_role | kick
  @type remove_role :: 0
  @type kick :: 1

  @typedoc "the grace period (in days) before expiring subscribers"
  @type expire_grace_period :: integer

  @typedoc "user for this integration"
  @type user :: User.t()

  @typedoc "integration account information"
  @type account :: Account.t()

  @typedoc "the ISO8601 timestamp when this integration was last synced"
  @type synced_at :: String.t()

  @typedoc "how many subscribers this integration has"
  @type subscriber_count :: integer

  @typedoc "has this integration been revoked"
  @type revoked :: boolean

  @typedoc "The bot/OAuth2 application for discord integrations"
  @type application :: Application.t()

  @type t :: %__MODULE__{
          id: id,
          name: name,
          type: type,
          enabled: enabled,
          syncing: syncing,
          role_id: role_id,
          enable_emoticons: enable_emoticons,
          expire_behavior: expire_behavior,
          expire_grace_period: expire_grace_period,
          user: user,
          account: account,
          synced_at: synced_at,
          subscriber_count: subscriber_count,
          revoked: revoked,
          application: application
        }
end
