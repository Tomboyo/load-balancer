defmodule LoadBalancer.Discord.Types.Guild.AuditLogRoleChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, Snowflake}

  defstruct [
    :id,
    :type,
    :permissions,
    :color,
    :hoist,
    :mentionable,
    :allow,
    :deny
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "permissions for a role changed"
  @type permissions :: String.t()

  @typedoc "role color changed"
  @type color :: integer()

  @typedoc "role is now displayed/no longer displayed separate from online users"
  @type hoist :: boolean()

  @typedoc "role is now mentionable/unmentionable"
  @type mentionable :: boolean()

  @typedoc "a permission on a text or voice channel was allowed for a role"
  @type allow :: String.t()

  @typedoc "a permission on a text or voice channel was denied for a role"
  @type deny :: String.t()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          permissions: permissions(),
          color: color(),
          hoist: hoist(),
          mentionable: mentionable(),
          allow: allow(),
          deny: deny()
        }
end
