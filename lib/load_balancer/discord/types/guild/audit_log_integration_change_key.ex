defmodule LoadBalancer.Discord.Types.Guild.AuditLogIntegrationChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, Snowflake}

  defstruct [
    :id,
    :type,
    :enable_emoticons,
    :expire_behavior,
    :expire_grace_period
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "integration emoticons enabled/disabled"
  @type enable_emoticons :: boolean()

  @typedoc "integration expiring subscriber behavior changed"
  @type expire_behavior :: integer()

  @typedoc "integration expire grace period changed"
  @type expire_grace_period :: integer()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          enable_emoticons: enable_emoticons(),
          expire_behavior: expire_behavior(),
          expire_grace_period: expire_grace_period()
        }
end
