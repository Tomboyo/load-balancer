defmodule LoadBalancer.Discord.Types.Guild.AuditLogUserChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, Snowflake}

  defstruct [
    :id,
    :type,
    :deaf,
    :mute,
    :nick,
    :avatar_hash
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "user server deafened/undeafened"
  @type deaf :: boolean()

  @typedoc "user server muted/unmuted"
  @type mute :: boolean()

  @typedoc "user nickname changed"
  @type nick :: String.t()

  @typedoc "user avatar changed"
  @type avatar_hash :: String.t()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          deaf: deaf(),
          mute: mute(),
          nick: nick(),
          avatar_hash: avatar_hash()
        }
end
