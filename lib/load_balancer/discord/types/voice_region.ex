defmodule LoadBalancer.Discord.Types.VoiceRegion do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/voice#voice-region-object)"

  defstruct [
    :id,
    :name,
    :vip,
    :optimal,
    :deprecated,
    :custom
  ]

  @typedoc "unique ID for the region"
  @type id :: String.t()

  @typedoc "name of the region"
  @type name :: String.t()

  @typedoc "true if this is a vip-only server"
  @type vip :: boolean

  @typedoc "true for a single server that is closest to the current user's client"
  @type optimal :: boolean

  @typedoc "whether this is a deprecated voice region (avoid switching to these)"
  @type deprecated :: boolean

  @typedoc "whether this is a custom voice region (used for events/etc)"
  @type custom :: boolean

  @type t :: %__MODULE__{
          id: id,
          name: name,
          vip: vip,
          optimal: optimal,
          deprecated: deprecated,
          custom: custom
        }
end
